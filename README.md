# API-Exercise

### Tech stack used
- __Python__ - Flask framework for API
- __MongoDB__ - for Database 
- __Docker-Compose__ - docker for containerizing and setup for local env
- __Kubernetes__ - used minikube for env setup

### Folder Details

| Folder Name | Details |
| ------ | ------ |
| api-layer | resources needed for the api routing |
| db | reources needed for database |
| Kube | contains manifest files for kubeneretes deployment and setup |

##### Approach taken for Database setup (task number 1)

MongoDB comes with __*mongoimport*__ command to import data into the DB. Used this command to seed the data from a separate container. The dockerfile present inside the db folder is configured for this purpose.
The image gets built and also spins up when ***Docker-compose*** command is used here.

##### Approach taken for Api creation (task number 2)

Python Flask framework has been used and all the necessary resources and code are inside the folder api-layer. API implemented from the api chart given in API.md Swagger is implemented for the documentation and testing purpose of the developed API.

##### Approach taken for Local Environment Setup (task number 3)
\
Used docker-compose to set up the environment which includes loading the seed data into mongoDB.
Below command brings up the environment and exposes to http://localhost:5555, when at the root directory of this repository where ***docker-compose.yml*** file can be seen the below commands are used consequently.
```sh
$ docker-compose build
```
The above command builds all the required docker images.
```sh
$ docker-compose up
```
The above command brings up the environment.

__p.s.__ - ideally the mongo-seed image will be outside this docker-compose because everytime ***docker-compose up*** is used data gets appended to the existing Database and created another set of collections.

##### Approach taken for Kubernetes deployment (task number 4)

Used **minikube** for the kubernetes cluster deployment. All the necessary manifest files are inside the *'kube'* folder.
Key details to note
- Application exposed at http://titanic-api.com with the use of **ingress**
- Used **statefulsets** for mongoDB to scale and maintain high availability
- Used **persistent volumes** for the mongoDB container.

Use the below command to deploy the objects to a kubernetes cluster when inside the '*kube*' folder in the repository.

```sh
$ kubectl create -f .
```
After an update to the manifest file use the below command
```sh
$ kubectl apply -f <file-name>
```
To update all files at one command please use the below
```sh
$ kubectl apply -f .
```
#
| File Name | Details |
| ------ | ------ |
| app-deploy.yml | This contains the deployment and service manifests for the api-layer |
| app-ingress.yml | contains ingress specifications for the app. Exposed as titanic-api.com |
| db-deploy.yml | contains statefulset deployment specifications and the required object specifications |
| db-pv.yml | contains the persistent volume specification which uses the default storage class from minikube |
| mongo-seed-job.yml | contains the job specification to load the seed data for the DB |

__p.s.__ : please note the below additional information for minikube setup
- enable ingress addons with '*minikube addons enable ingress*' command
- docker images are taken from local and to expose images to minikube use the command '*eval $(minikube -p minikube docker-env)*' before building an image
- map the ip address exposed in the ingress object to titanic-api.com in the /etc/hosts in the local.

![titanic-api on kubernetes](./titanic-api.png)

##### Approach taken for Improvements (task number 5)

Setup of Jenkins is done and instructions inside helm folder

Planned to setup the below
- CI-CD pipeline with helm charts
- security scan for the code and container
- unit tests for the code