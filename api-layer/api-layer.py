from flask import Flask, jsonify, request
from bson.json_util import dumps, loads
from bson.objectid import ObjectId
from pymongo import MongoClient
from flask_swagger_ui import get_swaggerui_blueprint


app = Flask("__name__")

swagger_url = '/swagger_ui'
_api_url = '/static/swagger.yml'
_swagger_ui_blueprint = get_swaggerui_blueprint(
    swagger_url,
    _api_url,
    config={
        'app_name': "Titanic-API"
    }
)
app.register_blueprint(_swagger_ui_blueprint, url_prefix=swagger_url)


db_client = MongoClient("mongodb://mongo-db:27017")
db = db_client.titanic

@app.route('/')
def root_path():    
    _all_people = db.mem_list.find().count()
    return str(_all_people)
    

@app.route('/people')
def get_people():
    _all_people = db.mem_list.find()
    _response = dumps(_all_people)
    return _response

@app.route('/people/<id>')
def get_people_by_id(id):
    __people = db.mem_list.find_one({'_id' : ObjectId(id)})
    _response = dumps(__people)
    return _response

@app.route('/people', methods=['POST'])
def add_people():

    values_from_request = request.get_json
    json_values = values_from_request([''])
    
    _survived = json_values['survived']
    _passengerClass = json_values['passengerClass']
    _name = json_values['name']
    _sex = json_values['sex']
    _age = json_values['age']
    _siblings0rSpouseAboard = json_values['siblingsOrSpousesAboard']
    _parents0rChildrenAboard = json_values['parentsOrChildrenAboard']
    _fare = json_values['fare']

    id = db.mem_list.insert_one({
        'survived' : _survived,
        'passengerClass' : _passengerClass,
        'name' : _name,
        'sex' : _sex,
        'age' : _age,
        'siblings0rSpouseAboard' : _siblings0rSpouseAboard,
        'parents0rChildrenAboard' : _parents0rChildrenAboard,
        'fare' : _fare
    })

    added_people_details = db.mem_list.find_one({'_id' : ObjectId(id.inserted_id)})
    _response = dumps(added_people_details)
    return _response

@app.route('/people/<id>', methods=['PUT'])
def update_people(id):

    values_from_request = request.get_json
    json_values = values_from_request([''])
    
    _survived = json_values['survived']
    _passengerClass = json_values['passengerClass']
    _name = json_values['name']
    _sex = json_values['sex']
    _age = json_values['age']
    _siblings0rSpouseAboard = json_values['siblingsOrSpousesAboard']
    _parents0rChildrenAboard = json_values['parentsOrChildrenAboard']
    _fare = json_values['fare']

    db.mem_list.update_one({
        '_id' : ObjectId(id['$oid']) if '$oid' in id else ObjectId(id)} ,
        {'$set' : {
        'survived' : _survived,
        'passengerClass' : _passengerClass,
        'name' : _name,
        'sex' : _sex,
        'age' : _age,
        'siblings0rSpouseAboard' : _siblings0rSpouseAboard,
        'parents0rChildrenAboard' : _parents0rChildrenAboard,
        'fare' : _fare
        }
    })
    
    _response = {
        "message" : "User with id : " + id +" has been updated successfully",
        "Status Code" : 200
    }
    return jsonify(_response)

@app.route('/people/<id>', methods=['DELETE'])
def delete_people_by_id(id):
    db.mem_list.delete_one({'_id' : ObjectId(id)})
    _response = {
        "message" : "User with id : " + id +" has been deleted successfully",
        "Status Code" : 200
    }
    return jsonify(_response)

@app.errorhandler(404)
def not_found(error=None):
    msg = {
        'Status Code' : 404,
        'message' : 'Not found '  + request.url
    }
    _response = jsonify(msg)
    _response.status_code = 404
    return _response

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)