# Status

Tried to implement the below, however was not able to complete it.
- CI-CD pipeline with helm charts
- security scan for the code and container
- unit tests for the code

Below is where I stopped as I hit the timelimit for this task

Deployment to minikube cluster failed becasue the jenkins elastic slave agent image did not have kubectl installed. Please look at screenshot below.

![Jenkins error](./Jenkins-Error.png)

Below was the high level plan that i wanted to implement as a pipeline on Jenkins

![Plan](./highlevel-Plan.png)
