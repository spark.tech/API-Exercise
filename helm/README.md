# Deploy Jenkins to Minikube Cluster

Follow the below steps to deploy Jenkins via helm when on the root folder of the repository

##### step1:
Execute the below commands after installing helm
```sh
$ helm repo add jenkinsci https://charts.jenkins.io/  
$ helm install jenkinsci/jenkins --version 2.13.2  --values helm/values.yaml --generate-name
```
All the configurations for jenkins is in the values.yaml file. ( used configuration as code and Job Dsl in jenkins)

##### step2:

Follow the instructions below to view jenkins on port 8080 on the browser
```
Get the Jenkins URL to visit by running these commands in the same shell:
$ export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/component=jenkins-master" -l "app.kubernetes.io/instance=jenkins-1603178122" -o jsonpath="{.items[0].metadata.name}")
$ kubectl --namespace default port-forward $POD_NAME 8080:8080
```

##### step3:

When jenkins is up use the below credentials to login
- username: admin
- password: admin

Below is the view after login

![Jenkins on Kubernetes](./Jenkins.png)

Please look at [Status.md](./Status.md) for current status and known issues